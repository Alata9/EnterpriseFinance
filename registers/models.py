from django.db import models

from directory.models import Currencies, Organization

from django.contrib.auth.models import User


class AccountSettings(models.Model):
    """
    The document stores information about the accounting settings in the organization and is filled out first.
    Accounting items to configure:
    multiple_accounts: makes it possible to keep records on several payment accounts,
    otherwise the records are carried out on one account.

    multiple_currencies: makes it possible to keep records in several currencies,
    otherwise it does not reflect the currencies in documents.

    multiple_projects: makes it possible to keep records of projects,
    otherwise it does not reflect projects in documents.

    organization_default: in a multi-organization system,
    the selected organization will be suggested by default in all documents.

    accounting_currency: with a multi-currency system, the selected currency will be offered by default
    in all documents. All reports and charts are translated in this currency.
    The accounting currency cannot be changed subsequently.

    multiple_organizations: makes it possible to keep records of several organizations in a company,
    otherwise records are carried out in one organization.
    """

    multiple_organizations = models.BooleanField("Keep records of multiple organizations", blank=False)
    multiple_accounts = models.BooleanField("Use multiple payment accounts", blank=False)
    multiple_currencies = models.BooleanField("Use multiple accounting currencies", blank=False)
    multiple_projects = models.BooleanField("Keep records of projects", blank=False)
    organization_default = models.ForeignKey(Organization, on_delete=models.PROTECT, blank=True)
    accounting_currency = models.ForeignKey(Currencies, on_delete=models.CASCADE, blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.PROTECT, null=True)

    def get_absolute_url(self):
        return "/settings"

    @classmethod
    def load(cls):
        try:
            return cls.objects.get(id=1)
        except:
            pass

        return cls(id=1)

    def organization(self):
        try:
            return self.organization_default
        except:
            ...
        return None

    def currency(self):
        try:
            return self.accounting_currency
        except:
            ...
        return None
