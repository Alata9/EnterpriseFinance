from django.contrib import admin

from registers.models import AccountSettings


class AccountSettingsAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "multiple_organizations",
        "multiple_accounts",
        "multiple_currencies",
        "multiple_projects",
        "organization_default",
        "accounting_currency",
        "user",
    ]
    list_display_links = [
        "multiple_organizations",
        "multiple_accounts",
        "multiple_currencies",
        "multiple_projects",
        "organization_default",
        "accounting_currency",
        "user",
    ]
    search_fields = [
        "multiple_organizations",
        "multiple_accounts",
        "multiple_currencies",
        "multiple_projects",
        "organization_default",
        "accounting_currency",
        "user",
    ]


admin.site.register(AccountSettings, AccountSettingsAdmin)
