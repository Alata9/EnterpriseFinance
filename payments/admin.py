from django.contrib import admin

from payments.models import PaymentDocuments


class PaymentDocumentsAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "user",
        "organization",
        "date",
        "outflow_amount",
        "inflow_amount",
        "account",
        "currency",
        "counterparty",
        "item",
        "project",
        "comments",
        "flow",
    ]
    list_display_links = [
        "id",
        "user",
        "organization",
        "date",
        "outflow_amount",
        "inflow_amount",
        "account",
        "currency",
        "counterparty",
        "item",
        "project",
        "comments",
        "flow",
    ]
    search_fields = [
        "date",
        "user",
        "flow",
        "organization",
        "counterparty",
        "account",
        "currency",
        "item",
        "project",
    ]


admin.site.register(PaymentDocuments, PaymentDocumentsAdmin)
