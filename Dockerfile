FROM python:3.11.7-alpine3.18

ENV PYTHONUNBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE=1

RUN apk update \
&& apk add postgresql-dev gcc python3-dev musl-dev

WORKDIR /app

COPY . .

RUN pip install --upgrade pip \
&& pip install poetry \
&& poetry config virtualenvs.create false \
&& poetry install

EXPOSE 8000

CMD ["sh", "run.sh"]
