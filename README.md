#  Enterprise finance 
#### (*on development stage*)

### *App for Treasury Department: accounting, budgeting and business analytics*
Includes:
- directories of organizations, counterparties, currencies, current accounts and the like,
- actual payment documents,
- scheduled payment documents,
- financial calculators,
- financial reports and consolidated budgets,
- dashboards with charts and performance metrics for various activities.

### Application functionality:
Implemented multicurrency accounting with conversion.\
It is possible to import currency rates from official websites.\
Maintenance of several organizations and accounts is available.\
It is possible to keep records for individual projects.\
Financial calculators have been introduced to calculate loan payments.

### Usability:
Ease of entering and editing documents.\
Convenient information search system with advanced filters.\
Implemented import and export for bulk loading of documents from MS Excel.\
The ability to quickly create groups of documents based on existing ones using the “create based on” method.

### Users and Security:
Information access groups have been organized that are regulated by the Administrator.\
User authorization is carried out with differentiation of access rights.

### Demo version and training:
To preview the application, an automatically updated Demo account has been created with a completed database where you can practice.

## Stack

![Python](https://img.shields.io/badge/Python-FFD43B?style=for-the-badge&logo=python&logoColor=blue)
![Python](https://img.shields.io/badge/Django-092E20?style=for-the-badge&logo=django&logoColor=green)
![Python](https://img.shields.io/badge/PostgreSQL-316192?style=for-the-badge&logo=postgresql&logoColor=white)\
![Python](https://img.shields.io/badge/%3C/%3E%20htmx-3D72D7?style=for-the-badge&logoColor=white)
![Python](https://img.shields.io/badge/HTML5-E34F26?style=for-the-badge&logo=html5&logoColor=white)
![Python](https://img.shields.io/badge/CSS3-1572B6?style=for-the-badge&logo=css3&logoColor=white)
![Python](https://img.shields.io/badge/Bootstrap-563D7C?style=for-the-badge&logo=bootstrap&logoColor=white)
![Python](https://img.shields.io/badge/Markdown-000000?style=for-the-badge&logo=markdown&logoColor=white)\
![Python](https://img.shields.io/badge/Pandas-2C2D72?style=for-the-badge&logo=pandas&logoColor=white)
![Python](https://img.shields.io/badge/json-5E5C5C?style=for-the-badge&logo=json&logoColor=white)
![Python](https://img.shields.io/badge/JavaScript-323330?style=for-the-badge&logo=javascript&logoColor=F7DF1E)
![Python](https://img.shields.io/badge/Google%20Charts-green?style=for-the-badge&logo=Google)\
![Python](https://img.shields.io/badge/redis-CC0000.svg?&style=for-the-badge&logo=redis&logoColor=white)
![Python](https://img.shields.io/badge/Celery-red?style=for-the-badge&logo=celery)
![Python](https://img.shields.io/badge/Docker-2CA5E0?style=for-the-badge&logo=docker&logoColor=white)
![Python](https://img.shields.io/badge/GIT-E44C30?style=for-the-badge&logo=git&logoColor=white)
![Python](https://img.shields.io/badge/Gitlab_CI/CD-E44?style=for-the-badge&logo=gitlab&logoColor=white)

## How to see it at work
[Pre-Alfa version of app here](http://findep.cloud)