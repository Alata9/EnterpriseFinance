SHELL=/bin/bash

export DJANGO_LOCAL_APPS := $(shell poetry run python -c "from finance.settings import LOCAL_APPS; print(' '.join(LOCAL_APPS))")
export DJANGO_SUPERUSER_USERNAME := "admin"
export DJANGO_SUPERUSER_PASSWORD := "admin"
export DJANGO_SUPERUSER_EMAIL := "admin@example.com"

description := "EnterpriseFinance django project"

.PHONY: all
all: about
	@echo
	@printf "%-12s %b\n" "tools" "\e[0;90mInstall required binaries locally\e[0m"
	@printf "\e[0;90m--------------------------------------------\e[0m\n"
	@printf "%-12s %b\n" "migrate" "\e[0;90mMake migrations for all installed apps and run migrations\e[0m"
	@printf "%-12s %b\n" "admin" "\e[0;90mCreate superuser '${DJANGO_SUPERUSER_USERNAME}'\e[0m"
	@printf "%-12s %b\n" "run" "\e[0;90mRuns Django project server\e[0m"
	@printf "%-12s %b\n" "celery" "\e[0;90mRuns Celery Worker\e[0m"
	@printf "%-12s %b\n" "beat" "\e[0;90mRuns Celery Beat\e[0m"
	@printf "\e[0;90m--------------------------------------------\e[0m\n"
	@printf "%-12s %b\n" "format" "\e[0;90mRun code formatters\e[0m"
	@printf "%-12s %b\n" "lint" "\e[0;90mRun linters\e[0m"
	@printf "%-12s %b\n" "test" "\e[0;90mRun tests\e[0m"
	@printf "\e[0;90m--------------------------------------------\e[0m\n"
	@printf "%-12s %b\n" "build" "\e[0;90mBuild docker container with project\e[0m"
	@printf "%-12s %b\n" "up" "\e[0;90mRun project with docker compose \e[0m"
	@printf "%-12s %b\n" "down" "\e[0;90mStop docker compose configured project\e[0m"
	@printf "%-12s %b\n" "clean" "\e[0;90mClean data from docker compose configured project\e[0m"
	@echo

.PHONY: about
about:
		@echo "$(description)"

.PHONY: tools
tools:
		@poetry add --dev ruff black

.PHONY: migrate
migrate:
		@echo "Making migrations for: ${DJANGO_LOCAL_APPS}" && \
			poetry run python ./manage.py makemigrations ${DJANGO_LOCAL_APPS} && \
			echo "" && \
			echo "Migrating..." && \
			poetry run python ./manage.py migrate

.PHONY: admin
admin:
		@echo "Creating superuser '${DJANGO_SUPERUSER_USERNAME}' with password '${DJANGO_SUPERUSER_PASSWORD}' and email '${DJANGO_SUPERUSER_EMAIL}'" && \
			DJANGO_SUPERUSER_USERNAME="${DJANGO_SUPERUSER_USERNAME}" \
		  	DJANGO_SUPERUSER_PASSWORD="${DJANGO_SUPERUSER_PASSWORD}" \
		  	DJANGO_SUPERUSER_EMAIL="${DJANGO_SUPERUSER_EMAIL}" \
		  	poetry run python ./manage.py createsuperuser --noinput

.PHONY: run
run:
		@poetry run python ./manage.py runserver

.PHONY: celery
celery:
		@poetry run celery -A finance worker -l INFO

.PHONY: beat
beat:
		@poetry run celery -A finance beat -l INFO

.PHONY: format
format:
		@echo "Running black..." && \
			poetry run black . && \
			echo "" && \
			echo "Running ruff..." && \
			poetry run ruff check --fix

.PHONY: lint
lint:
		@echo "Running ruff check..." && \
			poetry run ruff check .

.PHONY: test
test:
		@echo "Running tests..." && \
			DB_USER="postgres" DB_PASSWORD="postgres" poetry run python ./manage.py test -v 2

.PHONY: build
build:
		docker compose build app

.PHONY: up
up:
		docker compose up -d

.PHONY: down
down:
		docker compose down

.PHONY: clean
clean:
		docker compose down --rmi all
		#rm -rf ./app_data
