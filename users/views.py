from django.contrib.auth import logout
from django.views import View
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.models import User
from django.contrib.auth.views import LoginView, PasswordChangeView
from typing import Any
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import (
    UpdateView,
    CreateView,
    TemplateView,
    DeleteView,
)
from users.forms import ChangeUserInfoForm, RegisterUserForm, UserChangePasswordForm
from users.tasks import send_change_password_notification_task


# entry
def UserEntryView(request):
    return render(request, "users/entry.html")


# pending
def PendingView(request):
    return render(request, "users/pending.html")


# login
class UserLoginView(LoginView):
    template_name = "users/login.html"


# logout
class UserLogoutView(View):
    def get(self, request):
        logout(request)
        return redirect("home")


# change personal info
class ChangeUserInfoView(SuccessMessageMixin, LoginRequiredMixin, UpdateView):
    model = User
    template_name = "users/personal_account.html"
    form_class = ChangeUserInfoForm
    success_url = reverse_lazy("home")
    success_message = "Personal data has been successfully changed"

    def setup(self, request, *args, **kwargs):
        self.user_id = request.user.pk
        return super().setup(request, *args, **kwargs)

    def get_object(self, queryset=None):
        if not queryset:
            queryset = self.get_queryset()
        return get_object_or_404(queryset, pk=self.user_id)


# change password
class UserPasswordChangeView(SuccessMessageMixin, LoginRequiredMixin, PasswordChangeView):
    template_name = "users/password_change.html"
    form_class = UserChangePasswordForm
    success_url = reverse_lazy("home")
    success_message = _("Your password has been successfully changed")

    def get_context_data(self, **kwargs) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context["title"] = _("Changing the user's password")
        return context

    def form_valid(self, form):
        user = form.save()

        user_name = " ".join(filter(None, [user.first_name, user.last_name])) or user.username

        send_change_password_notification_task.delay(user.email, user_name)

        return super().form_valid(form)


# sign in
class RegisterUserView(CreateView):
    model = User
    template_name = "users/register_user.html"
    form_class = RegisterUserForm
    success_url = reverse_lazy("register_done")


class RegisterDoneView(TemplateView):
    template_name = "users/register_done.html"


# delete user
class DeleteUserView(LoginRequiredMixin, DeleteView):
    model = User
    template_name = "users/profile_delete.html"
    success_url = reverse_lazy("home")

    def setup(self, request, *args, **kwargs):
        self.user_id = request.user.pk
        return super().setup(request, *args, **kwargs)

    def get_object(self, queryset=None):
        if not queryset:
            queryset = self.get_queryset()
        return get_object_or_404(queryset, pk=self.user_id)
