from http import HTTPStatus
from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse

User = get_user_model()


class RegisterUserTestCase(TestCase):
    def setUp(self):
        # self.client = Client()
        self.data = {
            "username": "user_1",
            "email": "user1@sitewomen.ru",
            "first_name": "Ivan",
            "last_name": "Ivanov",
            "password1": "12345678Aa",
            "password2": "12345678Aa",
        }

    # корректное отражение формы при get-запросе
    def test_form_registration_get(self):
        path = reverse("access:signup")
        response = self.client.get(path)
        # correct url, status 200
        self.assertEqual(response.status_code, HTTPStatus.OK)
        # correct .html
        self.assertTemplateUsed(response, "access/signup.html")

    # успешная запись и сохранение в базе
    def test_user_registration_success(self):
        path = reverse("access:signup")
        response = self.client.post(path, self.data)
        # correct url, status 302
        self.assertEqual(response.status_code, HTTPStatus.FOUND)
        # correct redirect url
        self.assertRedirects(response, reverse("access:signupcomplite"))
        # correct add and save in base
        self.assertTrue(User.objects.filter(username=self.data["username"]).exists())

    # ошибка при дублировании пароля
    def test_user_registration_password_error(self):
        self.data["password2"] = "12345678A"

        path = reverse("access:signup")
        response = self.client.post(path, self.data)
        # correct url, status 200
        self.assertEqual(response.status_code, HTTPStatus.OK)
        # correct error-text
        self.assertContains(response, "The two password fields didn’t match")

    # ошибка неуникальный username
    def test_user_registration_user_exists_error(self):
        User.objects.create(username=self.data["username"])

        path = reverse("access:signup")
        response = self.client.post(path, self.data)
        # correct url, status 200
        self.assertEqual(response.status_code, HTTPStatus.OK)
        # correct error-text
        self.assertContains(response, "A user with that username already exists")


class GetProfileTest(TestCase):
    def setUp(self):
        # self.client = Client()
        self.data = {
            "username": "user_1",
            "email": "user1@sitewomen.ru",
            "first_name": "Ivan",
            "last_name": "Ivanov",
            "password": "12345678Aa",
        }
        self.user = User.objects.create_user(**self.data)

    def test_user_data(self):
        path = reverse("access:profile")

        # login
        logged = self.client.login(username=self.data["email"], password=self.data["password"])
        self.assertTrue(logged)

        response = self.client.get(path)
        self.assertEqual(response.context_data.get("user", User()).username, self.data["username"])

    def tearDown(self):
        pass
