from celery import shared_task
from django.conf import settings
from django.core.mail import EmailMessage
from django.utils.translation import gettext as _
from celery.utils.log import get_task_logger

logger = get_task_logger(__name__)


def send_email(subject, body, to):
    email = EmailMessage(
        subject=subject,
        body=body,
        from_email=settings.EMAIL_HOST_USER,
        to=[to],
    )
    email.send()


@shared_task
def send_change_password_notification_task(email: str, username: str, password: str):
    subject = _("Password Change Notification")

    notification_body = f"""
    Hi {username},
    Your password has been successfully changed.
    """

    send_email(subject, notification_body, email)
