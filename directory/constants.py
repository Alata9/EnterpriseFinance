# from django.contrib.auth.models import User

"""Constants for the model Items"""

"""Types of activity of the enterprise"""
FINANCING = "Financing"
INVESTING = "Investing"
OPERATING = "Operating"


"""Direction of cash flow"""
PAYMENTS = "Payments"
RECEIPTS = "Receipts"


"""Item groups"""
LOANS = "Credits and loans"
PROJECTS = "New projects"
FIXED_ASSETS = "Fixed assets"
OTHER = "Other income and expenses"
SALES_INCOME = "Sales income"
DIRECT_EXPENSES = "Direct expenses"
ADMIN_EXPENSES = "Administrative expenses"
COMMERCIAL_EXPENSES = "Commercial expenses"
PRODUCTION_COSTS = "Production costs"


"""System items"""
P_LOAN_ISSUANCE = "Loan issuance"
R_LOAN_ACQUISITION = "Loan acquisition"
P_OF_RECEIVED_LOAN = "Repayment of received loan"
R_OF_ISSUED_LOAN = "Repayment of issued loan"
P_NEW_PROJECT = "Investment in new projects"
P_FIXED_ASSETS = "Investment in fixed assets"
R_NEW_PROJECT = "Revenue from new projects"
P_OTHER_ASSETS = "Acquisition of securities and other assets"
R_OTHER_ASSETS = "Sale of securities and other assets"
R_LOANS_INTEREST = "Interest income from loans disbursed"
P_LOANS_INTEREST = "Interest payment on loans received"
R_OTHER_INCOME = "Other income"
P_OTHER_EXPENSES = "Other expenses"
CCA = "Change in current account"
