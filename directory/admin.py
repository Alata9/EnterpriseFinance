from django.contrib import admin

from directory.models import (
    Organization,
    PaymentAccount,
    Project,
    Counterparties,
    InitialDebts,
    Currencies,
    CurrenciesRates,
    Items,
)


class OrganizationAdmin(admin.ModelAdmin):
    list_display = ["id", "organization", "comments", "user"]
    list_display_links = ["id", "organization", "comments", "user"]
    search_fields = ["organization", "user"]


class PaymentAccountAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "account",
        "organization",
        "is_cash",
        "currency",
        "comments",
        "user",
    ]
    list_display_links = [
        "id",
        "account",
        "organization",
        "currency",
        "comments",
        "user",
    ]
    search_fields = ["account", "organization", "is_cash", "user"]


class ProjectAdmin(admin.ModelAdmin):
    list_display = ["id", "organization", "project", "user"]
    list_display_links = ["id", "organization", "project", "user"]
    search_fields = ["organization", "project", "user"]


class CurrenciesAdmin(admin.ModelAdmin):
    list_display = ["id", "currency", "code", "user"]
    list_display_links = ["id", "currency", "code", "user"]
    search_fields = ["currency", "code", "user"]


class CurrenciesRatesAdmin(admin.ModelAdmin):
    list_display = ["id", "accounting_currency", "currency", "date", "rate", "user"]
    list_display_links = [
        "id",
        "accounting_currency",
        "currency",
        "date",
        "rate",
        "user",
    ]
    search_fields = ["accounting_currency", "code", "user"]


class CounterpartiesAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "counterparty",
        "comments",
        "suppliers",
        "customer",
        "employee",
        "other",
        "user",
    ]
    list_display_links = [
        "id",
        "counterparty",
        "comments",
        "suppliers",
        "customer",
        "employee",
        "other",
        "user",
    ]
    search_fields = ["counterparty", "user"]


class InitialDebtsAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "counterparty",
        "comments",
        "organization",
        "type_debt",
        "debit",
        "credit",
        "currency",
        "user",
    ]
    list_display_links = [
        "id",
        "counterparty",
        "comments",
        "organization",
        "type_debt",
        "debit",
        "credit",
        "currency",
        "user",
    ]
    search_fields = ["counterparty", "organization", "type_debt", "user"]


class ItemsAdmin(admin.ModelAdmin):
    list_display = ["id", "code", "name", "group", "flow", "activity", "user"]
    list_display_links = ["id", "code", "name", "group", "flow", "activity", "user"]
    search_fields = ["code", "name", "user"]


admin.site.register(Organization, OrganizationAdmin)
admin.site.register(PaymentAccount, PaymentAccountAdmin)
admin.site.register(Project, ProjectAdmin)
admin.site.register(Currencies, CurrenciesAdmin)
admin.site.register(CurrenciesRates, CurrenciesRatesAdmin)
admin.site.register(Counterparties, CounterpartiesAdmin)
admin.site.register(InitialDebts, InitialDebtsAdmin)
admin.site.register(Items, ItemsAdmin)
