import os

from django.apps import AppConfig


class DirectoryappConfig(AppConfig):
    """Adding system items (expenses, income) before each application launch with existence check.
    System items are ITEMS from .directory/data.py.
    """

    default_auto_field = "django.db.models.BigAutoField"
    name = "directory"

    def ready(self):
        if os.environ.get("RUN_MAIN", None) == "true":
            from directory.models import Items

            Items.populate()
        return True
