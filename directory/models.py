from django.db import models
from django.contrib.auth.models import User
from directory.data import ITEMS
from directory.constants import (
    RECEIPTS,
    PAYMENTS,
    FINANCING,
    OPERATING,
    INVESTING,
    LOANS,
    PROJECTS,
    FIXED_ASSETS,
    OTHER,
    SALES_INCOME,
    DIRECT_EXPENSES,
    ADMIN_EXPENSES,
    COMMERCIAL_EXPENSES,
    PRODUCTION_COSTS,
)


class Organization(models.Model):
    """Basic information about the organization: name, general comments."""

    organization = models.CharField(max_length=100, unique=True)
    comments = models.CharField(max_length=250, blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.PROTECT, null=True)

    def __str__(self):
        return self.organization

    class Meta:
        ordering = ["organization"]


class PaymentAccount(models.Model):
    """
    Basic information about the payment account of organization:
    bank, owner organization, currency of accounting, cash account type (yes|no), general comments.
    """

    account = models.CharField(max_length=50, unique=True)
    organization = models.ForeignKey(Organization, on_delete=models.PROTECT, blank=False, null=False)
    is_cash = models.BooleanField(blank=False)
    currency = models.ForeignKey("Currencies", on_delete=models.PROTECT, blank=False, null=False)
    comments = models.CharField(max_length=100, blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.PROTECT, null=True)

    def __str__(self):
        return self.account

    class Meta:
        ordering = ["organization", "account"]
        verbose_name = "Payment account"
        verbose_name_plural = "Payment accounts"


class Project(models.Model):
    """Basic information about the project of organization: name, owner organization, general comments."""

    project = models.CharField(max_length=100, unique=True)
    organization = models.ForeignKey(Organization, on_delete=models.PROTECT, blank=False)
    comments = models.CharField(max_length=250, blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.PROTECT, null=True)

    def __str__(self):
        return self.project

    class Meta:
        ordering = ["organization", "project"]
        verbose_name = "Project"
        verbose_name_plural = "Projects"


class Counterparties(models.Model):
    """
    Basic information about the counterparty:
    name, type (supplier, customer, employee, other), general comments.
    """

    counterparty = models.CharField(max_length=100, unique=True)
    comments = models.CharField(max_length=100, blank=True, null=True)
    suppliers = models.BooleanField(blank=True, default=False)
    customer = models.BooleanField(blank=True, default=False)
    employee = models.BooleanField(blank=True, default=False)
    other = models.BooleanField(blank=True, default=False)
    user = models.ForeignKey(User, on_delete=models.PROTECT, null=True)

    def __str__(self):
        return self.counterparty

    class Meta:
        ordering = ["counterparty"]
        verbose_name = "Counterparty"
        verbose_name_plural = "Counterparties"


class InitialDebts(models.Model):
    """
    Basic information about the initial debts of counterparty:
    counterparty name, organization - counterparty, currency of debt, type of debt,
    amount of debt by debit or credit, general comments.
    """

    TypeDebts = (
        ("", ""),
        ("Lender", "Lender"),
        ("Borrower", "Borrower"),
        ("Other", "Other"),
    )

    counterparty = models.ForeignKey(Counterparties, on_delete=models.PROTECT, blank=False)
    organization = models.ForeignKey(Organization, on_delete=models.PROTECT, blank=False)
    debit = models.DecimalField(max_digits=15, decimal_places=2, default=0.0, blank=True, null=True)
    credit = models.DecimalField(max_digits=15, decimal_places=2, default=0.0, blank=True, null=True)
    comments = models.CharField(max_length=250, blank=True, null=True)
    currency = models.ForeignKey("Currencies", on_delete=models.PROTECT, blank=False)
    type_debt = models.CharField(max_length=10, choices=TypeDebts, blank=False)
    user = models.ForeignKey(User, on_delete=models.PROTECT, null=True)

    def __str__(self):
        return f"{self.counterparty}, {self.organization}, {self.type_debt}, DR: {self.debit}, CR: {self.credit}, {self.currency}"

    class Meta:
        ordering = ["counterparty", "organization"]
        verbose_name = "Counterparty initial debts"
        verbose_name_plural = "Counterparties initial debts"


class Currencies(models.Model):
    """Basic information about the currency of accounting: name and official currency code."""

    currency = models.CharField(max_length=10, unique=True)
    code = models.CharField(max_length=3, blank=False)
    user = models.ForeignKey(User, on_delete=models.PROTECT, null=True)

    def __str__(self):
        return self.code

    class Meta:
        ordering = ["currency"]
        verbose_name = "Currency"
        verbose_name_plural = "Currencies"


class Items(models.Model):
    """
    Directory of cash inflows and outflows. Each item has attributes:
    name: customers item name,
    code: the system item name, which is not available for the user to change,
    group: economic item group,
    flow: direction of cash flow by item on document (Receipts/Payments),
    activity: type of economic activity by item (operating/financial/investment)

    Some items are of a systemic nature and cannot be deleted by users.
    Lists of types of economic activities and groups of articles are permanent,
    unchangeable, and are not available to the user for modification or deletion.
    """

    FLOW_DIRECTION = (
        (RECEIPTS, RECEIPTS),
        (PAYMENTS, PAYMENTS),
    )
    ITEM_GROUPS = (
        (SALES_INCOME, SALES_INCOME),
        (DIRECT_EXPENSES, DIRECT_EXPENSES),
        (ADMIN_EXPENSES, ADMIN_EXPENSES),
        (COMMERCIAL_EXPENSES, COMMERCIAL_EXPENSES),
        (PRODUCTION_COSTS, PRODUCTION_COSTS),
        (OTHER, OTHER),
        (LOANS, LOANS),
        (FIXED_ASSETS, FIXED_ASSETS),
        (PROJECTS, PROJECTS),
    )

    ACTIVITIES = (
        (OPERATING, OPERATING),
        (FINANCING, FINANCING),
        (INVESTING, INVESTING),
    )

    GROUP_ACTIVITIES = {
        ITEM_GROUPS[0][0]: OPERATING,
        ITEM_GROUPS[1][0]: OPERATING,
        ITEM_GROUPS[2][0]: OPERATING,
        ITEM_GROUPS[3][0]: OPERATING,
        ITEM_GROUPS[4][0]: OPERATING,
        ITEM_GROUPS[5][0]: OPERATING,
        ITEM_GROUPS[6][0]: FINANCING,
        ITEM_GROUPS[7][0]: INVESTING,
        ITEM_GROUPS[8][0]: INVESTING,
    }

    @classmethod
    def get_activity(cls, group):
        activities = {
            1: [cls.ITEM_GROUPS[0:5]],
            2: [cls.ITEM_GROUPS[6]],
            3: [cls.ITEM_GROUPS[7:8]],
        }
        return [k for k in activities if set(activities[k]) & {group}]

    name = models.CharField(max_length=100, unique=True, blank=True, null=True)
    code = models.CharField(max_length=100, unique=True, blank=True, null=True)
    group = models.CharField(max_length=100, choices=ITEM_GROUPS, blank=False)
    flow = models.CharField(max_length=100, choices=FLOW_DIRECTION, blank=True)
    activity = models.CharField(max_length=100, choices=ACTIVITIES, blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.PROTECT, null=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["group", "name"]
        verbose_name = "Item"
        verbose_name_plural = "Items"

    def get_absolute_url(self):
        return "/items"

    @classmethod
    def populate(cls):
        admins = User.objects.filter(is_superuser=True).order_by("id")
        if not admins:
            return

        admin = admins[0]
        for item in ITEMS:
            if cls.objects.filter(code=item["code"]).count() > 0:
                continue
            obj = cls(**item)
            obj.user = admin
            obj.save()


class CurrenciesRates(models.Model):
    """
    Register for storing information about exchange rates.
    Each entry has information about the date, currency pair and official exchange rate.
    """

    date = models.DateField(blank=False)
    accounting_currency = models.ForeignKey(
        Currencies,
        related_name="cur1",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )
    currency = models.ForeignKey(
        Currencies,
        on_delete=models.CASCADE,
        related_name="cur2",
        blank=True,
        null=True,
    )
    rate = models.DecimalField(max_digits=15, decimal_places=6, blank=False, default=0.00)
    user = models.ForeignKey(User, on_delete=models.PROTECT, null=True)

    def __str__(self):
        return f"{self.accounting_currency}, {self.currency}, {self.date}, {self.rate}"

    class Meta:
        ordering = ["date", "accounting_currency", "currency"]
        verbose_name = "Rate"
        verbose_name_plural = "Currency rates"
