from celery import shared_task
from celery.utils.log import get_task_logger

logger = get_task_logger(__name__)


@shared_task
def reset_database():
    from django.core.management import execute_from_command_line

    execute_from_command_line(["", "flush", "--noinput"])
    execute_from_command_line(["", "loaddata", "db.json"])

    from directory.models import Items

    Items.populate()
