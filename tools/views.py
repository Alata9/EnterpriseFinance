from django.views import View
from django.http import HttpResponse


class DataLoadView(View):
    def get(self, request, *args, **kwargs):
        from django.core.management import execute_from_command_line

        execute_from_command_line(["", "flush", "--noinput"])
        execute_from_command_line(["", "loaddata", "db.json"])
        return HttpResponse("Ok")
