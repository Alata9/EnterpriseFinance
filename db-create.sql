DROP DATABASE IF EXISTS db_finance;
DROP ROLE IF EXISTS db_finance;

CREATE DATABASE db_finance ENCODING 'UTF-8' TEMPLATE template0;
CREATE ROLE db_finance WITH LOGIN PASSWORD 'db_finance';
ALTER DATABASE db_finance OWNER TO db_finance;
