import os
from celery import Celery
from celery.schedules import crontab

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "finance.settings")

app = Celery("finance")
app.config_from_object("django.conf:settings", namespace="CELERY")

app.autodiscover_tasks()


@app.task(bind=True, ignore_result=False)
def debug_task(self):
    print(f"Request: {self.request!r}")
    return "Task completed"


app.conf.beat_schedule = {
    "reset_database": {
        "task": "tools.tasks.reset_database",
        "schedule": crontab(hour=1, minute=0),
    }
}
